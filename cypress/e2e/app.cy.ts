const CBF_FILENAME = 'in16c_010001.cbf';

describe('App', () => {
  afterEach(() => {
    window.sessionStorage.clear();
  });

  it('show the home page', () => {
    cy.visit('/');
    cy.findByRole('heading', { name: 'Braggy' }).should('be.visible');
    cy.findByRole('button', { name: 'Log in' }).should('be.visible');
  });

  it('display a CBF file with rings', () => {
    cy.visit('/');

    cy.intercept(
      'GET',
      `/daiquiri/api/filebrowser/file?path=data%2F${CBF_FILENAME}`
    ).as('data_request');

    cy.findByRole('textbox', { name: 'Proposal' }).type('abcd');
    cy.findByRole('button', { name: 'Log in' }).click();

    cy.findByRole('button', { name: 'data' }).click();
    cy.findByRole('button', { name: CBF_FILENAME }).click();

    cy.wait('@data_request');
    cy.waitForStableDOM();

    cy.findByRole('button', { name: 'More controls' }).click();
    cy.findByRole('button', { name: 'Rings' }).click();
    cy.waitForStableDOM();

    cy.findByRole('figure', { name: CBF_FILENAME }).should('be.visible');
    cy.findByText('4.99 Å').should('be.visible');

    if (Cypress.env('TAKE_SNAPSHOTS')) {
      cy.matchImageSnapshot('cbf_file_rings');
    }
  });

  it('load a CBF file from the URL', () => {
    cy.visit(`/?file=data/${CBF_FILENAME}`);

    cy.intercept(
      'GET',
      `/daiquiri/api/filebrowser/file?path=data%2F${CBF_FILENAME}`
    ).as('data_request');

    cy.findByRole('textbox', { name: 'Proposal' }).type('abcd');
    cy.findByRole('button', { name: 'Log in' }).click();

    cy.wait('@data_request');
    cy.waitForStableDOM();

    cy.findByRole('figure', { name: CBF_FILENAME }).should('be.visible');
    cy.findByRole('button', { name: CBF_FILENAME }).should(
      'have.attr',
      'data-current'
    );
  });
});
