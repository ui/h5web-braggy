# Braggy

Braggy is a stand-alone, web-based application for browsing and visualizing
diffraction data served by a [Daiquiri](https://gitlab.esrf.fr/ui/daiquiri)
server. It is built with React and [H5Web](https://github.com/silx-kit/h5web).

![screenshot](/uploads/c434a77d981dd7178fd6d114e2b4c7bb/image.png)
