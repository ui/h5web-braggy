const { createConfig } = require('eslint-config-galex/dist/createConfig');
const { getDependencies } = require('eslint-config-galex/dist/getDependencies');
const {
  createJestOverride,
} = require('eslint-config-galex/dist/overrides/jest');
const {
  createReactOverride,
} = require('eslint-config-galex/dist/overrides/react');
const {
  createTypeScriptOverride,
} = require('eslint-config-galex/dist/overrides/typescript');

const dependencies = getDependencies();

module.exports = createConfig({
  enableJavaScriptSpecificRulesInTypeScriptProject: true,
  rules: {
    'sort-keys-fix/sort-keys-fix': 'off', // keys should be sorted based on significance
    'import/no-default-export': 'off', // default exports are common in React

    // Ternaries are sometimes more readable when `true` branch is most significant branch
    'no-negated-condition': 'off',
    'unicorn/no-negated-condition': 'off',

    // Prefer explicit, consistent return - e.g. `return undefined;`
    'unicorn/no-useless-undefined': 'off',
    'consistent-return': 'error',

    // Properties available after typeguard may be tedious to destructure (e.g. in JSX)
    'unicorn/consistent-destructuring': 'off',

    /* Forcing use of `else` for consistency with mandatory `default` clause in `switch` statements is unreasonable.
     * `if`/`else if` serves a different purpose than `switch`. */
    'sonarjs/elseif-without-else': 'off',

    // The point of `switch` is to be less verbose than if/else if/else
    'unicorn/switch-case-braces': ['warn', 'avoid'],

    // `import { type Foo }` requires TS 5.0's `verbatimModuleSyntax`, which causes issues with Jest
    // Sticking with `importsNotUsedAsValues` and `import type { Foo }` for now...
    'import/consistent-type-specifier-style': ['error', 'prefer-top-level'],

    // Galex currently disables checking for duplicate imports in a TS environment, even though TS doesn't warn about this
    'import/no-duplicates': 'error',
  },
  overrides: [
    createReactOverride({
      ...dependencies,
      rules: {
        'react/jsx-no-constructed-context-values': 'off', // too strict
        'react/no-unknown-property': 'off', // false positives with R3F

        // Suggests alternatives that are incorrect, deprecated or not at all equivalent
        'jsx-a11y/prefer-tag-over-role': 'off',
      },
    }),
    createTypeScriptOverride({
      ...dependencies,
      rules: {
        '@typescript-eslint/ban-ts-comment': 'off', // too strict
        '@typescript-eslint/lines-between-class-members': 'off', // allow grouping single-line members
        '@typescript-eslint/prefer-nullish-coalescing': 'off', // `||` is often convenient and safe to use with TS
        '@typescript-eslint/explicit-module-boundary-types': 'off', // worsens readability sometimes (e.g. for React components)
        '@typescript-eslint/no-unnecessary-type-arguments': 'off', // lots of false positives

        // Unused vars should be removed but not prevent compilation
        '@typescript-eslint/no-unused-vars': [
          'warn',
          { ignoreRestSiblings: true },
        ],

        // Allow writing void-returning arrow functions in shorthand to save space
        '@typescript-eslint/no-confusing-void-expression': [
          'error',
          { ignoreArrowShorthand: true },
        ],

        // Prefer `interface` over `type`
        '@typescript-eslint/consistent-type-definitions': [
          'error',
          'interface',
        ],

        // Disallows calling function with value of type `any` (disabled due to false positives)
        // Re-enabling because has helped fix a good number of true positives
        '@typescript-eslint/no-unsafe-argument': 'warn',

        '@typescript-eslint/consistent-type-assertions': [
          'error',
          {
            assertionStyle: 'as',
            objectLiteralTypeAssertions: 'allow', // `never` is too strict
          },
        ],

        // Disallow shadowing variables for an outer scope, as this can cause bugs
        // when the inner-scope variable is removed, for instance
        '@typescript-eslint/no-shadow': 'error',

        // Warn on deprecated APIs (TypeScript strikes them out but doesn't report them)
        'etc/no-deprecated': 'warn',
      },
    }),
    createJestOverride({
      ...dependencies,
      rules: {
        'jest/no-focused-tests': 'warn', // warning instead of error
        'jest/prefer-strict-equal': 'off', // `toEqual` is shorter and sufficient in most cases
        'jest-formatting/padding-around-all': 'off', // allow writing concise two-line tests
        'jest/require-top-level-describe': 'off', // filename should already be meaningful, extra nesting is unnecessary
        'jest/no-conditional-in-test': 'off', // false positives in E2E tests (snapshots), and too strict (disallows using `||` for convenience)
        'testing-library/no-unnecessary-act': 'off', // `act` is sometimes required when advancing timers manually
      },
    }),
    {
      files: ['**/*.cy.ts'],
      rules: {
        'sonarjs/no-duplicate-string': 'off', // flags @testing-library/cypress command strings as duplicates
      },
    },
  ],
});
