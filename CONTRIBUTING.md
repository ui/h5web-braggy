# Contributing

- [Quick start](#quick-start-)
- [Development](#build--test)
  - [`pnpm` cheat sheet](#pnpm-cheat-sheet)
  - [Dependency management](#dependency-management)
    - [DefinitelyTyped packages](#definitelytyped-packages)
- [Build & Test](#build--test)
  - [Visual regression](#visual-regression)
- [Code quality](#code-quality)
  - [Automatic fixing and formatting](#automatic-fixing-and-formatting)
  - [Editor integration](#editor-integration)
- [Deployment](#deployment)

## Quick start 🚀

Make sure you have [Node@18](https://nodejs.org/en/download/current/) and
[pnpm@8](https://pnpm.io/installation) installed, then run:

```bash
pnpm install
pnpm start
```

## Development

- `pnpm start` - start Braggy in development

### `pnpm` cheat sheet

- `pnpm install` - install the dependencies
- `pnpm add [-D] <pkg-name>` - add a dependency
- `pnpm [run] <script> [--<arg>]` - run a script
- `pnpm [exec] <binary>` - run a binary located in `node_modules/.bin`
  (equivalent to `npx <pkg-name>` for a package installed in the repo)
- `pnpm dlx <pkg-name>` - fetch a package from the registry and run its default
  command binary (equivalent to `npx <pkg-name>`)
- `pnpm why <pkg-name>` - show all packages that depend on the specified package
- `pnpm outdated` - list outdated dependencies
- `pnpm up -L <pkg-name>` - update a package to the latest version

### Dependency management

1. Run `pnpm outdated` to list dependencies that can be upgraded.
1. Read the changelogs and release notes of the dependencies you'd like to
   upgrade. Look for potential breaking changes, and for bug fixes and new
   features that may help improve the codebase.
1. Run `pnpm up -L <pkg-name>` to upgrade a dependency to the latest version.
   Alternatively, you can also edit `package.json` manually and run
   `pnpm install` (but make sure to specify an exact dependency version rather
   than a range - i.e. don't prefix the version with a caret or a tilde).

Note that some packages cannot be updated for the following reasons:

- `react` and `react-dom`: Some dependencies (incl. `@h5web/lib`) are not
  compatible with React 18.
- `@react-three/fiber`: `8.0.0` and higher are only compatible with React 18.
- `@testing-library/react`: `13.0.0` and higher are only compatible with
  React 18.

> If you run into peer dependency warnings and other package resolution issues,
> note that `pnpm` offers numerous solutions for dealing with them, like
> [`pnpm.peerDependencyRules.allowedVersions`](https://pnpm.io/package_json#pnpmpeerdependencyrulesallowedversions).

#### [DefinitelyTyped](https://github.com/DefinitelyTyped/DefinitelyTyped) packages

The major versions of `@types/*` packages must be aligned with the major
versions of the packages they provide types for—i.e. `foo@x.y.z` requires
`@types/foo@^x`.

For convenience, `@types` packages can be quickly upgraded to their latest
minor/patch version by running `pnpm up`.

## Build & Test

- `pnpm build` - build Braggy for production
- `pnpm test` - run unit and feature tests with Jest
- `pnpm cypress:open` - open the
  [Cypress](https://docs.cypress.io/guides/overview/why-cypress.html) end-to-end
  test runner (local dev server must be running in separate terminal)
- `pnpm cypress:run` - run end-to-end tests once (local dev server must be
  running in separate terminal)

### Visual regression

Cypress is used for end-to-end testing but also for visual regression testing.
The idea is to take a screenshot (or "snapshot") of the app in a known state and
compare it with a previously approved "reference snapshot". If any pixel has
changed, the test fails and a diff image highlighting the differences is
created.

Taking consistent screenshots across platforms is impossible because the exact
rendering of the app depends on the GPU. For this reason, visual regression
tests are run only on the CI. This is done through an environment variable
called `CYPRESS_TAKE_SNAPSHOTS`.

Visual regression tests may fail in the CI, either expectedly (e.g. when
implementing a new feature) or unexpectedly (when detecting a regression). When
this happens, the diff images and debug screenshots that Cypress generates are
uploaded as artifacts of the workflow, which can be downloaded and reviewed.

If the visual regressions are expected, the version-controlled reference
snapshots can be updated as follows:

1. Create and push a branch called `web` from your development branch.
2. Wait for the CI job `update_cypress_snapshots` to complete and download the
   updated snapshots from the job's artifacts.
3. Checkout your development branch then delete the `web` branches (both local
   and remote).
4. Replace the snapshots with the ones you downloaded from the CI.
5. Amend the latest commit of your development branch and force push.

## Code quality

- `pnpm lint` - run all linting and code formatting commands
- `pnpm lint:eslint` - lint all TS and JS files with ESLint
- `pnpm lint:tsc` - type-check the whole project, test files included
- `pnpm lint:prettier` - check that all files have been formatted with Prettier
- `pnpm analyze` - inspect the size and content of the JS bundles (after
  `pnpm build`)

### Automatic fixing and formatting

- `pnpm lint:eslint --fix` - auto-fix linting issues
- `pnpm lint:prettier --write` - format all files with Prettier

### Editor integration

Most editors support fixing and formatting files automatically on save. The
configuration for VSCode is provided out of the box, so all you need to do is
install the recommended extensions.

## Deployment

- The project's `main` branch is continuously deployed to GitLab Pages:
  https://ui.gitlab-pages.esrf.fr/h5web-braggy.
