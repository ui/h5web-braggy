import React, { useState } from 'react';
import { Alert, Button, Col, Container, Form, Row } from 'react-bootstrap';
import { BoxArrowUpRight } from 'react-bootstrap-icons';

import { logIn } from '../client/api';
import styles from './LoginPage.module.css';
import logo from './logo.png';
import { useUserToken } from './store';

export default function LoginPage() {
  const setToken = useUserToken((s) => s.setToken);
  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState(false);

  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = async (e: React.SyntheticEvent) => {
    e.preventDefault();
    setError(null);

    try {
      setLoading(true);
      const { token } = await logIn(username, password);
      setToken(token);
    } catch (error_) {
      const err = error_ as Error;
      setError(err.message);
    }
    setLoading(false);
  };

  return (
    <div className={styles.root}>
      <Container className="pt-3 pb-5">
        <Row className="align-items-center">
          <Col lg={8} xl={7}>
            <Row className="mb-3">
              <Col xs="auto" style={{ marginLeft: '-6px' }}>
                <img
                  className={styles.logo}
                  style={{ maxWidth: '100%' }}
                  src={logo}
                  width="150"
                  alt="ESRF - European Synchrotron Research Facility"
                />
              </Col>
              <Col>
                <h1 className="display-3 mb-3" style={{ marginTop: '-2px' }}>
                  Braggy
                </h1>
                <p className="fs-3 text-muted" style={{ maxWidth: '15em' }}>
                  Diffraction image visualisation in&nbsp;the&nbsp;browser
                </p>
                <p>
                  <a
                    className="link-dark"
                    style={{ whiteSpace: 'nowrap' }}
                    href="https://www.esrf.fr/braggy"
                    target="_blank"
                    rel="noreferrer"
                  >
                    Features overview
                    <BoxArrowUpRight
                      className="ms-2"
                      size="0.75em"
                      aria-label="opens in new window"
                    />
                  </a>{' '}
                  <span className="mx-2">|</span>{' '}
                  <a
                    className="link-dark"
                    style={{ whiteSpace: 'nowrap' }}
                    href="https://gitlab.esrf.fr/ui/h5web-braggy"
                    target="_blank"
                    rel="noreferrer"
                  >
                    GitLab repository
                    <BoxArrowUpRight
                      className="ms-2"
                      size="0.75em"
                      aria-label="opens in new window"
                    />
                  </a>
                </p>
              </Col>
            </Row>
          </Col>
          <Col>
            <h2 className="mb-4">Log in</h2>
            <Form noValidate onSubmit={(e) => void handleSubmit(e)}>
              {error && (
                <Alert variant="danger">
                  {`There was an error: ${error}. Please retry later.`}
                </Alert>
              )}

              <p>Please log in with your proposal account.</p>
              <Form.FloatingLabel
                controlId="username"
                className="mb-3"
                label="Proposal"
              >
                <Form.Control
                  type="text"
                  name="username"
                  required
                  placeholder="name@example.com"
                  onChange={({ target }) => setUserName(target.value)}
                />
                <Form.Control.Feedback type="invalid">
                  Please provide a proposal ID
                </Form.Control.Feedback>
              </Form.FloatingLabel>

              <Form.FloatingLabel
                controlId="password"
                className="mb-3"
                label="Password"
              >
                <Form.Control
                  type="password"
                  name="password"
                  required
                  placeholder="*****"
                  onChange={({ target }) => setPassword(target.value)}
                />
                <Form.Control.Feedback type="invalid">
                  Please provide a password
                </Form.Control.Feedback>
              </Form.FloatingLabel>

              <Button variant="dark" type="submit" disabled={loading}>
                {loading ? 'Logging in...' : 'Log in'}
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
