import { create } from 'zustand';
import { createJSONStorage, persist } from 'zustand/middleware';

interface State {
  token: string | undefined;
  setToken: (t: string) => void;
  resetToken: () => void;
}

export const useUserToken = create<State>()(
  persist(
    (set) => ({
      token: undefined,
      setToken: (token) => {
        set({ token });
      },
      resetToken: () => {
        set({ token: undefined });
      },
    }),
    {
      name: 'braggy-auth',
      storage: createJSONStorage(() => sessionStorage),
      version: 1,
    }
  )
);

export const getToken = () => useUserToken.getState().token;
export const clearToken = useUserToken.persist.clearStorage;
