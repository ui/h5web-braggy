export interface User {
  username: string;
  last_access: number;
  operator: boolean;
  sessionid: string;
}

export interface UserData {
  token: string;
  user: User;
}
