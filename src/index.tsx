import 'react-app-polyfill/stable';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-reflex/styles.css';
import '@h5web/lib/styles.css';
import './index.css';

import { assertNonNull } from '@h5web/lib';
import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import { ErrorBoundary } from 'react-error-boundary';
import { SWRConfig } from 'swr';

import App from './app/App';
import ErrorFallback from './app/ErrorFallback';

const rootElem = document.querySelector('#root');
assertNonNull(rootElem);

createRoot(rootElem).render(
  <StrictMode>
    <ErrorBoundary
      FallbackComponent={ErrorFallback}
      onReset={() => window.location.reload()}
    >
      <SWRConfig value={{ suspense: true }}>
        <App />
      </SWRConfig>
    </ErrorBoundary>
  </StrictMode>
);
