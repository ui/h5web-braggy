import type { RenderResult } from '@testing-library/react';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { SWRConfig } from 'swr';

import {
  fetchBraggyFile,
  fetchDirectory,
  fetchRootPath,
  logIn,
} from '../client/api';
import { useSocketCallback } from '../filebrowser/hooks';
import { useUserToken } from '../login/store';
import App from './App';
import type { BraggyFile, Entry } from './models';

export const mockLogIn = jest.mocked(logIn);
export const mockFetchRootPath = jest.mocked(fetchRootPath);
export const mockFetchDirectory = jest.mocked(fetchDirectory);
export const mockFetchBraggyFile = jest.mocked(fetchBraggyFile);
export const mockUseSocketCallback = jest.mocked(useSocketCallback);

interface RenderOptions {
  withFakeSession?: boolean;
}

interface RenderAppResult extends RenderResult {
  user: ReturnType<typeof userEvent.setup>;
}

export function renderApp(opts: RenderOptions = {}): RenderAppResult {
  const user = userEvent.setup();
  const { withFakeSession = true } = opts;

  if (withFakeSession) {
    useUserToken.setState({ token: 'abc' });
  }

  const renderResult = render(
    <SWRConfig value={{ suspense: true, provider: () => new Map() }}>
      <App />
    </SWRConfig>
  );

  return { user, ...renderResult };
}

export async function waitForAllLoaders(): Promise<void> {
  await waitFor(() => {
    expect(screen.queryAllByTestId(/^Loading/u)).toHaveLength(0);
  });
}

export function mockBraggyFile(entry: Entry): BraggyFile {
  return {
    hdr: {
      braggy_hdr: {
        img_width: 2,
        img_height: 2,
        pixel_size_x: 0.1,
        pixel_size_y: 0.1,
        min: 1,
        max: 4,
        mean: 2.5,
        positive_min: 1,
        strict_positive_min: 1,
        std: 4,
        detector_distance: 0.3,
        wavelength: 1,
        beam_ocx: 0,
        beam_ocy: 0,
      },
    },
    data: Float32Array.from([1, 2, 3, 4]),
    histogram: {
      values: [1, 1, 1, 1],
      bins: [0.5, 1.5, 2.5, 3.5, 4.5, 5.5],
      shape: [5],
      max: 1,
    },
    ...entry,
  };
}
