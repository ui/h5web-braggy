import { create } from 'zustand';

interface AppState {
  rootPath: string;
  directoryPath: string;
  imagePath: string | undefined;
  setRootPath: (path: string) => void;
  setDirectoryPath: (path: string) => void;
  setImagePath: (path: string) => void;
}

export const useAppStore = create<AppState>((set) => ({
  rootPath: '',
  directoryPath: '.',
  imagePath: undefined,
  setRootPath: (path) => set({ rootPath: path }),
  setDirectoryPath: (path) => set({ directoryPath: path }),
  setImagePath: (path) => set({ imagePath: path }),
}));
