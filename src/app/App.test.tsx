import { screen } from '@testing-library/react';

import {
  mockFetchDirectory,
  mockFetchRootPath,
  mockLogIn,
  renderApp,
} from './test-utils';

test('show login screen', async () => {
  renderApp({ withFakeSession: false });
  expect(screen.getByRole('button', { name: 'Log in' })).toBeVisible();
});

test('log in and out', async () => {
  const { user } = renderApp({ withFakeSession: false });

  mockFetchRootPath.mockResolvedValueOnce('/foo');
  mockLogIn.mockResolvedValueOnce({
    token: '123',
    user: {
      username: 'abcd',
      last_access: 0,
      operator: false,
      sessionid: '1234',
    },
  });
  mockFetchDirectory.mockResolvedValueOnce([]);

  await user.type(screen.getByLabelText('Proposal'), 'foo');
  await user.type(screen.getByLabelText('Password'), 'bar');

  await user.click(screen.getByRole('button', { name: 'Log in' }));
  expect(mockLogIn).toHaveBeenCalledWith('foo', 'bar');

  const logoutBtn = screen.getByRole('button', { name: 'Log out' });
  expect(logoutBtn).toBeVisible();
  expect(mockFetchDirectory).toHaveBeenCalledWith('.');

  await user.click(logoutBtn);
  expect(screen.getByRole('button', { name: 'Log in' })).toBeVisible();
});
