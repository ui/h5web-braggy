import { BrowserRouter, Route, Routes } from 'react-router-dom';

import LoginRoute from './LoginRoute';
import MainRoute from './MainRoute';

export default function App() {
  return (
    <BrowserRouter basename={process.env.REACT_APP_ROUTER_BASE_DIR}>
      <Routes>
        <Route path="login" element={<LoginRoute />} />
        <Route path="/" element={<MainRoute />} />
      </Routes>
    </BrowserRouter>
  );
}
