import { Suspense, useState } from 'react';
import { GripVertical } from 'react-bootstrap-icons';
import { ErrorBoundary } from 'react-error-boundary';
import { ReflexContainer, ReflexElement, ReflexSplitter } from 'react-reflex';
import { useSWRConfig } from 'swr';

import ImageLoader from '../imageview/ImageLoader';
import ImageViewContainer from '../imageview/ImageViewContainer';
import Menu from '../menu/Menu';
import MenuSidePanel from '../menu/MenuSidePanel';
import { Panel } from '../menu/models';
import ErrorFallback from './ErrorFallback';
import styles from './MainPage.module.css';
import { useAppStore } from './store';

function MainPage() {
  const imagePath = useAppStore((state) => state.imagePath);
  const { cache } = useSWRConfig();

  const [toolbarElem, setToolbarElem] = useState<HTMLDivElement>();
  const [activePanel, setActivePanel] = useState<Panel | undefined>(
    Panel.Files
  );

  function togglePanel(panel: Panel) {
    setActivePanel(activePanel !== panel ? panel : undefined);
  }

  if (!('ResizeObserver' in window)) {
    throw new Error(
      "Your browser's version is not supported. Please use a more recent browser version."
    );
  }

  return (
    <div className={styles.root}>
      <Menu activePanel={activePanel} onTogglePanel={togglePanel} />

      <ReflexContainer className={styles.container} orientation="vertical">
        <ReflexElement
          className={styles.panel}
          aria-label={activePanel}
          style={{ display: activePanel ? undefined : 'none' }}
          flex={20}
          minSize={200}
        >
          <MenuSidePanel activePanel={activePanel} />
        </ReflexElement>

        <ReflexSplitter
          className={styles.handle}
          style={{ display: activePanel ? undefined : 'none' }}
        >
          <GripVertical preserveAspectRatio="xMidYMid slice" />
        </ReflexSplitter>

        <ReflexElement className={styles.content} flex={80} minSize={500}>
          <div
            ref={(elem) => setToolbarElem(elem ?? undefined)}
            className={styles.toolbar}
          />

          {imagePath ? (
            <ErrorBoundary
              fallbackRender={(fallbackProps) => (
                <ErrorFallback path={imagePath} {...fallbackProps} />
              )}
              resetKeys={[imagePath]}
              onError={() => cache.delete(imagePath)}
            >
              <Suspense fallback={<ImageLoader />}>
                <ImageViewContainer
                  path={imagePath}
                  toolbarContainer={toolbarElem}
                />
              </Suspense>
            </ErrorBoundary>
          ) : (
            <p style={{ padding: '1.5rem' }}>Please select a file.</p>
          )}
        </ReflexElement>
      </ReflexContainer>
    </div>
  );
}

export default MainPage;
