import { AxiosError } from 'axios';
import type { PropsWithChildren } from 'react';
import { Button } from 'react-bootstrap';
import type { FallbackProps } from 'react-error-boundary';
import { Navigate } from 'react-router-dom';

import styles from './ErrorFallback.module.css';

function prepareReport(message: string, path: string | undefined): string {
  return `Hi,

  I encountered the following error in Braggy:

  - ${message}

  Here is some additional context:

  - User agent: ${navigator.userAgent}
  ${path ? `- Accessed path: ${path}` : ''}
  - << Please provide as much information as possible (beamline, file or dataset accessed, etc.) >>

  Thanks,
  << Name >>`;
}

interface Props extends FallbackProps {
  path?: string;
  variant?: string;
}

function ErrorFallback(props: PropsWithChildren<Props>) {
  const { error, path, variant, resetErrorBoundary, children } = props;

  if (error instanceof AxiosError && error.response?.status === 401) {
    return <Navigate to="login" />;
  }

  const message = error instanceof Error ? error.message : 'Unknown error';

  return (
    <div className={styles.root} role="alert" data-variant={variant}>
      <p>Something went wrong:</p>
      <pre className={styles.error}>{message}</pre>
      <p>
        <Button
          className={styles.tryAgainBtn}
          variant="info"
          onClick={() => resetErrorBoundary()}
        >
          Try again
        </Button>{' '}
        <Button
          variant="dark"
          target="_blank"
          href={`mailto:braggy@esrf.fr?subject=Error%20report&body=${encodeURIComponent(
            prepareReport(message, path)
          )}`}
        >
          Report issue
        </Button>
      </p>
      {children}
    </div>
  );
}

export type { Props as ErrorFallbackProps };
export default ErrorFallback;
