import { Navigate, useLocation } from 'react-router-dom';

import { useUserToken } from '../login/store';
import MainPage from './MainPage';

function MainRoute() {
  const location = useLocation();
  const token = useUserToken((s) => s.token);

  if (!token) {
    return <Navigate to="login" state={{ from: location }} replace />;
  }

  return <MainPage />;
}

export default MainRoute;
