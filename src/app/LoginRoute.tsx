import { Navigate, useLocation } from 'react-router-dom';

import { assertLocationObj } from '../filebrowser/utils';
import LoginPage from '../login/LoginPage';
import { useUserToken } from '../login/store';

function LoginRoute() {
  const { state } = useLocation();
  const token = useUserToken((s) => s.token);

  if (!token) {
    return <LoginPage />;
  }

  if (state) {
    const { from } = state;
    assertLocationObj(from);
    return (
      <Navigate to={`${from.pathname}${from.search}${from.hash}`} replace />
    );
  }

  return <Navigate to="/" replace />;
}

export default LoginRoute;
