import { usePrevious, useSet } from '@react-hookz/web';
import { useEffect } from 'react';
import { ListGroup, OverlayTrigger, Tooltip } from 'react-bootstrap';

import type { Entry } from '../app/models';
import { useAppStore } from '../app/store';
import EntryIcon from './EntryIcon';
import styles from './FileBrowser.module.css';

interface Props {
  entries: Entry[];
  numHiddenEntries: number;
  onEntryClick: (file: Entry) => void;
  onBackToParent: (() => void) | undefined;
}

function EntryListGroup(props: Props) {
  const { entries, numHiddenEntries, onEntryClick, onBackToParent } = props;

  const imagePath = useAppStore((state) => state.imagePath);
  const newEntriesPaths = useSet<string>();

  const prevEntries = usePrevious(entries);

  useEffect(() => {
    if (!prevEntries) {
      return; // skip first render so existing entries aren't marked as new
    }

    entries
      .filter((next) => !prevEntries.some((prev) => next.path === prev.path))
      .forEach((newEntry) => {
        newEntriesPaths.add(newEntry.path);
      });
  }, [entries, prevEntries, newEntriesPaths]);

  return (
    <ListGroup className={styles.list} role="list">
      {onBackToParent && (
        <ListGroup.Item
          className={styles.item}
          action
          onClick={() => onBackToParent()}
        >
          <EntryIcon type="dir" />
          ..
        </ListGroup.Item>
      )}
      {entries.map((entry) => (
        <OverlayTrigger
          key={entry.path}
          placement="right"
          delay={{ show: 750, hide: 150 }}
          overlay={
            <Tooltip id="path-tooltip" className={styles.pathTooltip}>
              {entry.name}
            </Tooltip>
          }
        >
          <ListGroup.Item
            key={entry.path}
            className={styles.item}
            action
            data-current={entry.path === imagePath || undefined}
            data-highlight={newEntriesPaths.has(entry.path) || undefined}
            onClick={() => onEntryClick(entry)}
          >
            <EntryIcon type={entry.type} />
            {entry.name}
          </ListGroup.Item>
        </OverlayTrigger>
      ))}
      {numHiddenEntries > 0 && (
        <ListGroup.Item className={styles.hintItem}>
          <EntryIcon type="dir" />
          {numHiddenEntries} hidden
        </ListGroup.Item>
      )}
    </ListGroup>
  );
}

export default EntryListGroup;
