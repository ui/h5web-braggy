import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

import { useAppStore } from '../app/store';
import SocketIOClient from '../client/SocketIOClient';
import { getParentDirectory } from './utils';

// Socket namespace
const NAMESPACE = 'filebrowser';

export function useSocketCallback(
  callbackName: string,
  callback: (payload: unknown) => void,
  deps: React.DependencyList
) {
  useEffect(() => {
    SocketIOClient.addNamespace(NAMESPACE);
    SocketIOClient.addCallback(NAMESPACE, callbackName, callback);
    SocketIOClient.openSockets();

    return () => {
      SocketIOClient.removeCallbacks(NAMESPACE, callbackName);
      SocketIOClient.closeSockets();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [callbackName, ...deps]);
}

export function useLoadPathFromUrl() {
  const query = new URLSearchParams(useLocation().search);
  const initialPath = query.get('file');

  const setDirectoryPath = useAppStore((state) => state.setDirectoryPath);
  const setImagePath = useAppStore((state) => state.setImagePath);

  useEffect(() => {
    if (initialPath) {
      setImagePath(initialPath);
      setDirectoryPath(getParentDirectory(initialPath));
    }
  }, [initialPath, setDirectoryPath, setImagePath]);
}
