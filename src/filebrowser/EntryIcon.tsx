import type { Icon } from 'react-bootstrap-icons';
import { FileEarmarkText, Folder, Image } from 'react-bootstrap-icons';

import styles from './FileBrowser.module.css';

const ENTRY_ICONS: Record<string, Icon> = {
  dir: Folder,
  image: Image,
  text: FileEarmarkText,
};

function EntryIcon({ type }: { type: string }) {
  const IconComponent = ENTRY_ICONS[type] || Image;

  return (
    <span className={styles.entityIcon}>
      <IconComponent />
    </span>
  );
}

export default EntryIcon;
