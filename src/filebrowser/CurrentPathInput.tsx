import { FormControl, OverlayTrigger, Tooltip } from 'react-bootstrap';

import styles from './FileBrowser.module.css';

interface Props {
  path: string;
}

function CurrentPathInput(props: Props) {
  const { path } = props;

  return (
    <OverlayTrigger
      placement="bottom"
      transition={false}
      delay={{ show: 750, hide: 150 }}
      overlay={
        <Tooltip id="path-tooltip" className={styles.pathTooltip}>
          {path}
        </Tooltip>
      }
    >
      <FormControl
        className={styles.pathInput}
        style={{ direction: 'rtl' }} // To have ellipsis at the beginning
        value={path.slice(1)} // Remove leading slash that gets pushed at the end with direction:rtl
        aria-label="Current path"
        disabled
      />
    </OverlayTrigger>
  );
}

export default CurrentPathInput;
