import type { PropsWithChildren } from 'react';
import { Button } from 'react-bootstrap';

import type { ErrorFallbackProps } from '../app/ErrorFallback';
import ErrorFallback from '../app/ErrorFallback';

interface Props extends ErrorFallbackProps {
  onBackToParent: (() => void) | undefined;
}

function DirectoryErrorFallback(props: PropsWithChildren<Props>) {
  const { onBackToParent, ...fallbackProps } = props;

  return (
    <ErrorFallback variant="browser" {...fallbackProps}>
      {onBackToParent && (
        <Button variant="info" onClick={() => onBackToParent()}>
          Go back to previous folder
        </Button>
      )}
    </ErrorFallback>
  );
}
export default DirectoryErrorFallback;
