import type { Location } from 'react-router-dom';

export function getParentDirectory(path: string) {
  if (!path.includes('/')) {
    return '.';
  }

  return path.split('/').slice(0, -1).join('/');
}

export function assertLocationObj(
  location: unknown
): asserts location is Location {
  if (!location || typeof location !== 'object' || !('pathname' in location)) {
    throw new Error('Expected location object');
  }
}
