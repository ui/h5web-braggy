import { useMap } from '@react-hookz/web';
import { Suspense, useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { ErrorBoundary } from 'react-error-boundary';
import { useSWRConfig } from 'swr';

import type { Entry } from '../app/models';
import { useAppStore } from '../app/store';
import { fetchRootPath } from '../client/api';
import { useFileBrowserConfig } from './config';
import CurrentPathInput from './CurrentPathInput';
import DirectoryErrorFallback from './DirectoryErrorFallback';
import DirectoryLoader from './DirectoryLoader';
import EntryListGroupContainer from './EntryListGroupContainer';
import styles from './FileBrowser.module.css';
import FilterInput from './FilterInput';
import { assertIsFileCreationEvent } from './guards';
import { useLoadPathFromUrl, useSocketCallback } from './hooks';
import SortInput from './SortInput';
import { getParentDirectory } from './utils';

export default function FileBrowser() {
  const rootPath = useAppStore((state) => state.rootPath);
  const directoryPath = useAppStore((state) => state.directoryPath);
  const setRootPath = useAppStore((state) => state.setRootPath);
  const setDirectoryPath = useAppStore((state) => state.setDirectoryPath);
  const setImagePath = useAppStore((state) => state.setImagePath);

  const { sortBy, setSortBy } = useFileBrowserConfig();

  const { cache, mutate } = useSWRConfig();

  useLoadPathFromUrl();
  useEffect(() => {
    if (!rootPath) {
      // eslint-disable-next-line promise/prefer-await-to-then
      fetchRootPath().then((path) => {
        setRootPath(path);
      });
    }
  }, [rootPath, setRootPath]);

  useSocketCallback(
    'new_file_in_dir',
    (payload) => {
      assertIsFileCreationEvent(payload);
      const { dir_path: dirPath } = payload;
      mutate(dirPath);
    },
    [mutate]
  );

  const filters = useMap<string, string>();
  const [numEntries, setNumEntries] = useState(0);
  const [numMatchingEntries, setNumMatchingEntries] = useState(0);

  const isRoot = directoryPath === '.';
  const absPath = isRoot ? rootPath : `${rootPath}/${directoryPath}`;
  const currentFilter = filters.get(directoryPath) || '';

  function pickEntry(path: string, type: string) {
    if (type === 'dir') {
      setDirectoryPath(path);
    } else {
      setImagePath(path);
    }
  }

  function handleBackToParent() {
    pickEntry(getParentDirectory(directoryPath), 'dir');
  }

  return (
    <div className={styles.root}>
      <div className={styles.status}>
        <SortInput value={sortBy} onChange={setSortBy} />
      </div>
      <div className={styles.inner}>
        <CurrentPathInput path={absPath} />

        <FilterInput
          value={currentFilter}
          onChange={(val) => filters.set(directoryPath, val)}
        />

        <ErrorBoundary
          fallbackRender={(fallbackProps) => (
            <DirectoryErrorFallback
              path={directoryPath}
              onBackToParent={isRoot ? undefined : handleBackToParent}
              {...fallbackProps}
            />
          )}
          resetKeys={[directoryPath]}
          onError={
            directoryPath ? () => cache.delete(directoryPath) : undefined
          }
        >
          <Suspense fallback={<DirectoryLoader />}>
            <EntryListGroupContainer
              path={directoryPath}
              onEntryClick={(file: Entry) => pickEntry(file.path, file.type)}
              onDirectoryFetch={(numEntriesInDir, numMatchingEntriesinDir) => {
                setNumEntries(numEntriesInDir);
                setNumMatchingEntries(numMatchingEntriesinDir);
              }}
              filter={currentFilter}
              sortBy={sortBy}
              onBackToParent={isRoot ? undefined : handleBackToParent}
            />
          </Suspense>
        </ErrorBoundary>
      </div>

      <div className={styles.status}>
        <p>
          {currentFilter.length > 0 && `${numMatchingEntries} of `}
          {numEntries} item{numEntries > 1 ? 's' : ''}
        </p>
        <Button
          className={styles.refreshBtn}
          variant="link"
          onClick={() => {
            mutate(directoryPath);
            pickEntry(directoryPath, 'dir');
          }}
        >
          Refresh
        </Button>
      </div>
    </div>
  );
}
