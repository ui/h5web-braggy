import { create } from 'zustand';
import { persist } from 'zustand/middleware';

import type { SortingMethod } from './models';

interface BrowserSettings {
  sortBy: SortingMethod;
  setSortBy: (s: SortingMethod) => void;
}

export const useFileBrowserConfig = create<BrowserSettings>()(
  persist(
    (set) => ({
      sortBy: 'name',
      setSortBy: (sortBy: SortingMethod) => set({ sortBy }),
    }),
    {
      name: 'braggy:filebrowser',
      version: 2,
    }
  )
);
