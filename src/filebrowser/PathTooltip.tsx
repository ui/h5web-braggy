import { Tooltip } from 'react-bootstrap';

function PathTooltip({ path, ...props }: { path: string }) {
  return (
    <Tooltip id="button-tooltip" {...props}>
      {path}
    </Tooltip>
  );
}

export default PathTooltip;
