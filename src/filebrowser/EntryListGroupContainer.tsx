import { useEffect } from 'react';

import type { Entry } from '../app/models';
import { useDirectory } from '../client/hooks';
import EntryListGroup from './EntryListGroup';
import type { SortingMethod } from './models';

const nameCollator = new Intl.Collator(undefined, { numeric: true });

interface Props {
  path: string;
  filter: string;
  sortBy: SortingMethod;
  onEntryClick: (file: Entry) => void;
  onDirectoryFetch: (numEntries: number, numMatchingEntries: number) => void;
  onBackToParent: (() => void) | undefined;
}

function EntryListGroupContainer(props: Props) {
  const {
    path,
    filter,
    sortBy,
    onEntryClick,
    onDirectoryFetch,
    onBackToParent,
  } = props;

  function compareEntries(a: Entry, b: Entry): number {
    if (a.type !== b.type) {
      // Put directories before files
      return a.type === 'dir' ? -1 : 1;
    }

    if (sortBy === 'time') {
      // Descending sorting
      return b.last_modified - a.last_modified;
    }

    return nameCollator.compare(a.name, b.name);
  }

  const entries = useDirectory(path);

  const words = filter.trim().toLowerCase().split(/\s+/u);
  const matchingEntries = entries
    .filter((entry) => words.every((w) => entry.name.toLowerCase().includes(w)))
    .sort(compareEntries);

  const numEntries = entries.length;
  const numMatchingEntries = matchingEntries.length;

  useEffect(() => {
    onDirectoryFetch(numEntries, numMatchingEntries);
  }, [numEntries, numMatchingEntries, onDirectoryFetch]);

  return (
    <EntryListGroup
      key={path}
      entries={matchingEntries}
      numHiddenEntries={numEntries - numMatchingEntries}
      onEntryClick={onEntryClick}
      onBackToParent={onBackToParent}
    />
  );
}

export default EntryListGroupContainer;
