import { screen, within } from '@testing-library/react';

import {
  mockFetchDirectory,
  mockFetchRootPath,
  mockUseSocketCallback,
  renderApp,
} from '../app/test-utils';

test('Display current path', async () => {
  mockFetchRootPath.mockResolvedValueOnce('/foo');
  mockFetchDirectory.mockResolvedValueOnce([]);

  renderApp();

  await expect(screen.findByDisplayValue('foo')).resolves.toBeVisible();
  expect(mockFetchRootPath).toHaveBeenCalledWith();
  expect(mockFetchDirectory).toHaveBeenCalledWith('.');
});

test("List files and directories at user's root path", async () => {
  mockFetchRootPath.mockResolvedValueOnce('/foo');
  mockFetchDirectory.mockResolvedValueOnce([
    {
      ext: '',
      name: 'dir1',
      path: './dir1',
      type: 'dir',
      last_modified: 1_632_131_166.866_78,
    },
    {
      ext: 'h5',
      name: 'file1',
      path: './file1',
      type: 'file',
      last_modified: 1_632_131_167.866_78,
    },
  ]);

  renderApp();

  await expect(screen.findByText('2 items')).resolves.toBeVisible();
  expect(screen.getByRole('button', { name: /dir1/ })).toBeVisible();
  expect(screen.getByRole('button', { name: /file1/ })).toBeVisible();
  expect(mockFetchRootPath).toHaveBeenCalledWith();
  expect(mockFetchDirectory).toHaveBeenCalledWith('.');
});

test('Navigate to sub-directory', async () => {
  mockFetchRootPath.mockResolvedValueOnce('/foo');
  mockFetchDirectory
    .mockResolvedValueOnce([
      {
        ext: '',
        name: 'dir1',
        path: 'dir1',
        type: 'dir',
        last_modified: 1_632_131_166.866_78,
      },
    ])
    .mockResolvedValueOnce([
      {
        ext: 'h5',
        name: 'file1',
        path: 'file1',
        type: 'file',
        last_modified: 1_632_131_167.866_78,
      },
    ]);

  const { user } = renderApp();

  await user.click(await screen.findByRole('button', { name: /dir1/ }));
  expect(screen.getByDisplayValue('foo/dir1')).toBeVisible();
  expect(screen.getByRole('button', { name: /file1/ })).toBeVisible();
  expect(mockFetchDirectory).toHaveBeenLastCalledWith('dir1');
});

test('Filter directory items', async () => {
  mockFetchRootPath.mockResolvedValueOnce('/foo');
  mockFetchDirectory.mockResolvedValueOnce([
    {
      ext: '',
      name: 'dir1',
      path: 'dir1',
      type: 'dir',
      last_modified: 1_632_131_166.866_78,
    },
    {
      ext: '',
      name: 'dir2',
      path: 'dir2',
      type: 'dir',
      last_modified: 1_632_131_167.866_78,
    },
    {
      ext: 'h5',
      name: 'file1',
      path: 'file1',
      type: 'file',
      last_modified: 1_632_131_168.866_78,
    },
    {
      ext: 'h5',
      name: 'file2',
      path: 'file2',
      type: 'file',
      last_modified: 1_632_131_169.866_78,
    },
  ]);

  const { user } = renderApp();
  await expect(screen.findByText(/4 items/)).resolves.toBeVisible();

  const filterInput = screen.getByLabelText('Filter');
  await user.type(filterInput, 'dir');
  await expect(screen.findByText('2 hidden')).resolves.toBeVisible();
  expect(screen.getByText('2 of 4 items')).toBeVisible();
  expect(screen.queryAllByRole('button', { name: /^dir/u })).toHaveLength(2);
  expect(
    screen.queryByRole('button', { name: /^file/u })
  ).not.toBeInTheDocument();

  await user.click(screen.getByRole('button', { name: 'Clear' }));
  await expect(screen.findByText('4 items')).resolves.toBeVisible();
  expect(screen.queryByText(/hidden/)).not.toBeInTheDocument();
  expect(filterInput).toHaveValue('');

  await user.type(filterInput, '2 dir');
  await expect(screen.findByText('3 hidden')).resolves.toBeVisible();
  expect(screen.getByText('1 of 4 items')).toBeVisible();
  expect(screen.getByRole('button', { name: /^dir2/u })).toBeVisible();
});

test('Navigate to parent folder with `..` entry but only for non-root folders', async () => {
  mockFetchRootPath.mockResolvedValueOnce('/root');
  mockFetchDirectory
    .mockResolvedValueOnce([
      {
        ext: '',
        name: 'dir1',
        path: 'dir1',
        type: 'dir',
        last_modified: 1_632_131_167.866_78,
      },
    ])
    .mockResolvedValueOnce([
      {
        ext: '',
        name: 'dir2',
        path: 'dir1/dir2',
        type: 'dir',
        last_modified: 1_632_131_168.866_78,
      },
    ]);

  const { user } = renderApp();
  await expect(screen.findByLabelText('Current path')).resolves.toContainHTML(
    'root'
  );
  expect(screen.queryByRole('button', { name: '..' })).not.toBeInTheDocument();

  // Navigate inside dir1
  await user.click(await screen.findByRole('button', { name: 'dir1' }));
  await expect(screen.findByLabelText('Current path')).resolves.toContainHTML(
    'root/dir1'
  );
  const parentDirButton = screen.getByRole('button', { name: '..' });
  expect(parentDirButton).toBeVisible();

  // Navigate back to parent folder (root)
  await user.click(parentDirButton);
  await expect(screen.findByLabelText('Current path')).resolves.toContainHTML(
    'root'
  );
});

test('Ignore `..` when counting and filtering directory items', async () => {
  mockFetchRootPath.mockResolvedValueOnce('/foo');
  mockFetchDirectory
    .mockResolvedValueOnce([
      {
        ext: '',
        name: 'dir1',
        path: 'dir1',
        type: 'dir',
        last_modified: 1_632_131_167.866_78,
      },
    ])
    .mockResolvedValueOnce([
      {
        ext: '',
        name: 'dir2',
        path: 'dir1/dir2',
        type: 'dir',
        last_modified: 1_632_131_168.866_78,
      },
    ]);

  const { user } = renderApp();

  // Navigate inside dir1 to trigger the apparition of `..` entry
  await user.click(await screen.findByRole('button', { name: 'dir1' }));
  await expect(screen.findByText(/1 item/)).resolves.toBeVisible();
  const filterInput = screen.getByLabelText('Filter');

  await user.type(filterInput, 'foo');
  await expect(screen.findByText('1 hidden')).resolves.toBeVisible();
  expect(screen.getByText('0 of 1 item')).toBeVisible();
  expect(screen.getByRole('button', { name: '..' })).toBeVisible();
  expect(
    screen.queryByRole('button', { name: 'dir2' })
  ).not.toBeInTheDocument();
});

test('Refresh directory', async () => {
  mockFetchRootPath.mockResolvedValueOnce('/foo');
  mockFetchDirectory.mockResolvedValueOnce([]).mockResolvedValueOnce([
    {
      ext: 'h5',
      name: 'file1',
      path: 'file1',
      type: 'file',
      last_modified: 1_632_131_166.866_78,
    },
  ]);

  const { user } = renderApp();
  await expect(screen.findByText('0 item')).resolves.toBeVisible();

  await user.click(screen.getByRole('button', { name: 'Refresh' }));
  await expect(screen.findByText('1 item')).resolves.toBeVisible();
});

test('Auto-refresh on new file and highlight', async () => {
  mockFetchRootPath.mockResolvedValueOnce('/foo');
  mockFetchDirectory.mockResolvedValueOnce([
    {
      ext: 'cbf',
      name: 'file1',
      path: 'file1',
      type: 'file',
      last_modified: 1_632_131_166.866_78,
    },
  ]);
  mockFetchDirectory.mockResolvedValueOnce([
    {
      ext: 'cbf',
      name: 'file1',
      path: 'file1',
      type: 'file',
      last_modified: 1_632_131_166.866_78,
    },
    {
      ext: 'cbf',
      name: 'file2',
      path: 'file2',
      type: 'file',
      last_modified: 1_632_131_167.866_78,
    },
  ]);

  renderApp();
  await expect(screen.findByText('1 item')).resolves.toBeVisible();
  expect(screen.getByText('Please select a file.')).toBeVisible();

  // Get callback arg from last call
  const { calls: socketHookCalls } = mockUseSocketCallback.mock;
  const socketCallback = socketHookCalls[socketHookCalls.length - 1][1];

  // Check that the folder is refreshed
  socketCallback({ dir_path: '.', file: 'file2.cbf' });
  await expect(screen.findByText('2 items')).resolves.toBeVisible();

  const newFileBtn = screen.getByRole('button', { name: /file2/ });
  expect(newFileBtn).toBeVisible();
  expect(newFileBtn).toHaveAttribute('data-highlight');

  // Check that the file was not loaded
  expect(screen.getByText('Please select a file.')).toBeVisible();
});

test('Sort by name and most recent', async () => {
  mockFetchRootPath.mockResolvedValueOnce('/foo');
  mockFetchDirectory.mockResolvedValueOnce([
    {
      ext: '',
      name: 'dir_old',
      path: 'dir_old',
      type: 'dir',
      last_modified: 1_632_131_165,
    },
    {
      ext: 'cbf',
      name: 'file_recent',
      path: 'file_recent',
      type: 'file',
      last_modified: 1_632_131_169,
    },
    {
      ext: 'cbf',
      name: 'file_old',
      path: 'file_old',
      type: 'file',
      last_modified: 1_632_131_168,
    },
    {
      ext: '',
      name: 'dir_recent',
      path: 'dir_recent',
      type: 'dir',
      last_modified: 1_632_131_166,
    },
  ]);

  const { user } = renderApp();
  const fileListing = await screen.findByRole('list');

  // Sorted by name by default
  expect(screen.getByRole('radio', { name: 'Sort by name' })).toBeChecked();

  // Expect directory first and then lexicographic order
  let items = within(fileListing).getAllByRole('button');
  expect(items[0]).toContainHTML('dir_old');
  expect(items[1]).toContainHTML('dir_recent');
  expect(items[2]).toContainHTML('file_old');
  expect(items[3]).toContainHTML('file_recent');

  // Sort by most recent
  await user.click(screen.getByRole('radio', { name: 'Sort by most recent' }));

  // Expect directory first and then most recent
  items = within(fileListing).getAllByRole('button');
  expect(items[0]).toContainHTML('dir_recent');
  expect(items[1]).toContainHTML('dir_old');
  expect(items[2]).toContainHTML('file_recent');
  expect(items[3]).toContainHTML('file_old');
});
