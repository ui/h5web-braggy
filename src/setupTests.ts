import '@testing-library/jest-dom';

jest.mock('zustand');

jest.mock('./client/api');
jest.mock('./filebrowser/hooks');

class ResizeObserver {
  public observe() {
    /* do nothing */
  }
  public unobserve() {
    /* do nothing */
  }
  public disconnect() {
    /* do nothing */
  }
}

window.ResizeObserver = ResizeObserver;

// Fake properties to avoid Re-flex warnings
// https://github.com/leefsmp/Re-Flex/issues/27
// https://github.com/jsdom/jsdom/issues/135
['offsetWidth', 'offsetHeight'].forEach((prop) => {
  Object.defineProperty(HTMLElement.prototype, prop, {
    configurable: true,
    value: 500,
  });
});

// Clear session and localStorage before each test
beforeEach(() => {
  window.localStorage.clear();
  window.sessionStorage.clear();
});
