import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { BoxArrowLeft, Files, Megaphone } from 'react-bootstrap-icons';

import { useUserToken } from '../login/store';
import styles from './Menu.module.css';
import { Panel } from './models';

interface Props {
  activePanel: Panel | undefined;
  onTogglePanel: (panel: Panel) => void;
}

export default function Menu(props: Props) {
  const { activePanel, onTogglePanel } = props;
  const resetToken = useUserToken((s) => s.resetToken);

  return (
    <div className={styles.menu}>
      <Button
        variant="link"
        aria-label="Toggle file panel"
        data-active={activePanel === Panel.Files || undefined}
        onClick={() => onTogglePanel(Panel.Files)}
      >
        <Files />
      </Button>
      <OverlayTrigger
        placement="right"
        delay={{ show: 250, hide: 0 }}
        overlay={<Tooltip id="logout-tooltip">Log out</Tooltip>}
      >
        <Button
          className={styles.logoutBtn}
          variant="link"
          aria-label="Log out"
          onClick={() => {
            resetToken();
          }}
        >
          <BoxArrowLeft />
        </Button>
      </OverlayTrigger>
      <OverlayTrigger
        placement="right"
        delay={{ show: 250, hide: 0 }}
        overlay={<Tooltip id="feedback-tooltip">Leave feedback</Tooltip>}
      >
        <Button
          variant="link"
          href="mailto:braggy@esrf.fr?subject=Feedback"
          target="_blank"
          aria-label="Feedback"
        >
          <Megaphone />
        </Button>
      </OverlayTrigger>
    </div>
  );
}
