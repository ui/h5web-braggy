import { screen } from '@testing-library/react';

import {
  mockFetchDirectory,
  mockFetchRootPath,
  renderApp,
} from '../app/test-utils';

test('toggle file browser', async () => {
  mockFetchRootPath.mockResolvedValueOnce('/foo');
  mockFetchDirectory.mockResolvedValueOnce([]);

  const { user } = renderApp();

  // Check that file browser is open by default
  expect(screen.getByLabelText('Current path')).toBeVisible();

  // Toggle off file browser
  await user.click(screen.getByRole('button', { name: /Toggle file/ }));
  expect(screen.queryByLabelText('Current path')).not.toBeInTheDocument();
});
