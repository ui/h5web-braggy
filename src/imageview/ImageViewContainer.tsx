import { useBraggyFile } from '../client/hooks';
import ImageView from './ImageView';

interface Props {
  path: string;
  toolbarContainer: HTMLDivElement | undefined;
}

export default function ImageViewContainer(props: Props) {
  const { path, toolbarContainer } = props;
  const image = useBraggyFile(path);

  return (
    <ImageView
      key={path} // avoid flash and stale domain when switching dataset
      data={image.data}
      braggyHeader={image.hdr.braggy_hdr}
      histogram={image.histogram}
      path={path}
      toolbarContainer={toolbarContainer}
    />
  );
}
