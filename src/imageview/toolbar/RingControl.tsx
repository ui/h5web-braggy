import { Selector, ToggleBtn } from '@h5web/lib';
import { Bullseye } from 'react-bootstrap-icons';

import { RingColor } from '../models';
import styles from './RingControl.module.css';

interface Props {
  showRings: boolean;
  toggleRings: () => void;
  ringColor: RingColor;
  getCssRingColor: (ringColor: RingColor) => string;
  setRingColor: (ringColor: RingColor) => void;
}

function RingControl(props: Props) {
  const { showRings, toggleRings, ringColor, getCssRingColor, setRingColor } =
    props;

  return (
    <div className={styles.container}>
      <ToggleBtn
        value={showRings}
        onToggle={toggleRings}
        label="Rings"
        icon={Bullseye}
      />

      <Selector
        value={ringColor}
        options={{
          'B & W': [RingColor.Black, RingColor.White],
          'Color map': [RingColor.ColorMapMin, RingColor.ColorMapMax],
        }}
        onChange={setRingColor}
        optionComponent={({ option, isSelected }) => (
          <div className={styles.option}>
            <div
              className={styles.square}
              style={{
                backgroundColor: getCssRingColor(option),
              }}
            />
            {!isSelected && (
              <span className={styles.optionLabel}>{option}</span>
            )}
          </div>
        )}
        disabled={!showRings}
      />
    </div>
  );
}

export default RingControl;
