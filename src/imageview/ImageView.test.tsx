import { screen } from '@testing-library/react';

import {
  mockBraggyFile,
  mockFetchBraggyFile,
  mockFetchDirectory,
  mockFetchRootPath,
  renderApp,
  waitForAllLoaders,
} from '../app/test-utils';

test('View image', async () => {
  const fileEntry = {
    ext: 'h5',
    name: 'file1',
    path: './file1',
    type: 'file',
    last_modified: 1_632_131_166.866_78,
  };

  mockFetchRootPath.mockResolvedValueOnce('/foo');
  mockFetchDirectory.mockResolvedValueOnce([fileEntry]);
  mockFetchBraggyFile.mockResolvedValueOnce(mockBraggyFile(fileEntry));

  const { user } = renderApp();

  const fileBtn = await screen.findByRole('button', { name: 'file1' });
  await user.click(fileBtn);
  await waitForAllLoaders();

  expect(screen.getByRole('figure')).toBeVisible();
});
