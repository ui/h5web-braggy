import { useUnmountEffect } from '@react-hookz/web';
import { ProgressBar } from 'react-bootstrap';

import { useProgressStore } from '../client/progress-store';
import styles from './ImageLoader.module.css';

function ImageLoader() {
  const progress = useProgressStore((state) => state.progress);
  const resetProgress = useProgressStore((state) => state.resetProgress);

  useUnmountEffect(() => resetProgress());

  return (
    <div className={styles.root} data-testid="Loading image">
      <div className={styles.grid}>
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
      </div>

      <ProgressBar now={progress} variant="info" />
    </div>
  );
}

export default ImageLoader;
