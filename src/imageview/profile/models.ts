import type { Rect } from '@h5web/lib';

export interface PersistedSelection {
  selection: Rect;
  shape: 'line' | 'circle';
}
