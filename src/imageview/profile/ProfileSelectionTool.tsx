import {
  DataToHtml,
  SelectionTool,
  SvgCircle,
  SvgElement,
  SvgLine,
} from '@h5web/lib';
import { useState } from 'react';
import type { Vector2 } from 'three';

import { CIRCLE_PROFILE_KEY, LINE_PROFILE_KEY } from '../utils';
import type { PersistedSelection } from './models';
import { pointsCrossedByCircle, pointsCrossedByLine } from './utils';

interface Props {
  color: string;
  onSelection: (points: Vector2[]) => void;
}

function ProfileSelectionTool(props: Props) {
  const { color, onSelection } = props;
  const [lastSelection, setLastSelection] = useState<PersistedSelection>();

  return (
    <>
      <SelectionTool
        id="LineSelection"
        modifierKey={LINE_PROFILE_KEY}
        onSelectionStart={() => setLastSelection(undefined)}
        onValidSelection={({ data: selection }) => {
          onSelection(pointsCrossedByLine(...selection));
          setLastSelection({ selection, shape: 'line' });
        }}
      >
        {({ html: selection }) => (
          <SvgElement>
            <SvgLine coords={selection} stroke={color} />
          </SvgElement>
        )}
      </SelectionTool>

      <SelectionTool
        id="CircleSelection"
        modifierKey={CIRCLE_PROFILE_KEY}
        onSelectionStart={() => setLastSelection(undefined)}
        onValidSelection={({ data: selection }) => {
          onSelection(pointsCrossedByCircle(...selection));
          setLastSelection({ selection, shape: 'circle' });
        }}
      >
        {({ html: selection }) => (
          <SvgElement>
            <SvgCircle coords={selection} stroke={color} fill="none" />
          </SvgElement>
        )}
      </SelectionTool>

      {lastSelection && (
        <DataToHtml points={lastSelection.selection}>
          {(...coords) => (
            <SvgElement>
              {lastSelection.shape === 'line' && (
                <SvgLine coords={coords} stroke={color} />
              )}
              {lastSelection.shape === 'circle' && (
                <SvgCircle coords={coords} stroke={color} fill="none" />
              )}
            </SvgElement>
          )}
        </DataToHtml>
      )}
    </>
  );
}

export default ProfileSelectionTool;
