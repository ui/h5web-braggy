import { act } from 'react-dom/test-utils';
import type { create as createType, StateCreator } from 'zustand';

const zustand = jest.requireActual('zustand');
const actualCreate: typeof createType = zustand.create;

const storeResetFns = new Set<() => void>();

function createImpl<S>(createState: StateCreator<S>) {
  const store = actualCreate(createState);
  const initialState = store.getState();
  storeResetFns.add(() => store.setState(initialState, true));
  return store;
}

export function create<S>(f: StateCreator<S> | undefined) {
  return f === undefined ? createImpl : createImpl(f);
}

beforeEach(() => {
  act(() => storeResetFns.forEach((resetFn) => resetFn()));
});
