import { create } from 'zustand';

type SetProgress = (progress: number) => void;

interface ProgressState {
  progress: number;
  setProgress: SetProgress;
  resetProgress: () => void;
}

export const useProgressStore = create<ProgressState>((set) => ({
  progress: 0,
  setProgress: (progress) => set({ progress }),
  resetProgress: () => set({ progress: 0 }),
}));
