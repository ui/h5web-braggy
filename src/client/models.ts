import type { Entry } from '../app/models';

export interface DirectoryResponse {
  abs_path: string;
  rows: Entry[];
  total: number;
}

export interface UserResponse {
  data: {
    username: string;
  };
  last_access: number;
  operator: boolean;
  sessionid: string;
  token: string;
}

export interface FileCreationEvent {
  dir_path: string;
  file: string;
}
