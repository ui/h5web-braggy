import useSWRImmutable from 'swr/immutable';

import type { BraggyFile, Entry } from '../app/models';
import { fetchBraggyFile, fetchDirectory } from './api';
import { useProgressStore } from './progress-store';

export function useDirectory(path: string): Entry[] {
  const { data } = useSWRImmutable<Entry[]>(path, fetchDirectory);

  if (data === undefined) {
    throw new Error('Expected response data to be defined');
  }

  return data.map((entry) => ({
    ...entry,
    name: entry.name.replace(/\.h5\.dataset$/u, ''),
  }));
}

export function useBraggyFile(path: string): BraggyFile {
  const setProgress = useProgressStore((state) => state.setProgress);

  const { data } = useSWRImmutable<BraggyFile, unknown, string>(path, (p) =>
    fetchBraggyFile(p, setProgress)
  );

  if (data === undefined) {
    throw new Error('Expected response data to be defined');
  }

  return data;
}
