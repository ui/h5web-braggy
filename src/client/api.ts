import type { AxiosHeaders, AxiosProgressEvent } from 'axios';
import axios from 'axios';
import qs from 'qs';

import type {
  BraggyFile,
  BraggyMetadata,
  Entry,
  HistMetadata,
} from '../app/models';
import type { UserData } from '../login/models';
import { clearToken, getToken } from '../login/store';
import type { DirectoryResponse, UserResponse } from './models';

const client = axios.create({
  baseURL: `${process.env.REACT_APP_DAIQUIRI_URL || ''}/api`,
  paramsSerializer: {
    serialize: (ps) => qs.stringify(ps, { arrayFormat: 'repeat' }),
  },
});

// Add `Authorization` header to every request if user is logged in
client.interceptors.request.use((config) => {
  const token = getToken();
  return token
    ? {
        ...config,
        headers: {
          ...config.headers,
          Authorization: `Bearer ${token}`,
        } as unknown as AxiosHeaders, // https://github.com/axios/axios/issues/5476
      }
    : config;
});

// If user is not authorized, clear token (redirect to login page is handled by ErrorFallback)
// eslint-disable-next-line promise/prefer-await-to-callbacks
client.interceptors.response.use(undefined, (error) => {
  if (error.response?.status === 401) {
    clearToken();
  }

  return Promise.reject(error);
});

export async function fetchDirectory(path: string): Promise<Entry[]> {
  const params = { path };

  const { data } = await client.get<DirectoryResponse>(
    '/filebrowser/directory',
    { params }
  );

  return data.rows;
}

export async function fetchBraggyFile(
  path: string,
  onProgress: (val: number) => void
): Promise<BraggyFile> {
  const params = { path };

  const [
    { data: metadataResponse },
    { data: contentResponse },
    { data: histResponse },
    { data: histMetaResponse },
  ] = await Promise.all([
    client.get<BraggyMetadata>('/filebrowser/loadfile', { params }),
    client.get<ArrayBuffer>('/filebrowser/file', {
      params,
      responseType: 'arraybuffer',
      onDownloadProgress: (evt: AxiosProgressEvent) => {
        const { loaded, total } = evt;
        if (total) {
          onProgress((loaded / total) * 100);
        }
      },
    }),
    client.get<ArrayBuffer>('/filebrowser/hist', {
      params,
      responseType: 'arraybuffer',
    }),
    client.get<HistMetadata>('/filebrowser/loadhist', { params }),
  ]);

  return {
    ...metadataResponse,
    data: new Float32Array(contentResponse),
    histogram: {
      values: [...new Float32Array(histResponse)],
      ...histMetaResponse,
    },
  };
}

export async function logIn(
  username: string,
  password: string
): Promise<UserData> {
  const { data: userResponse } = await client.post<UserResponse>(
    '/authenticator/login',
    {
      client: 'web',
      username,
      password,
    }
  );

  const { data, token, ...user } = userResponse;
  return { token, user: { username, ...user } };
}

export async function fetchRootPath(): Promise<string> {
  const params = { path: '.' };

  const { data } = await client.get<DirectoryResponse>(
    '/filebrowser/directory',
    { params }
  );
  const { abs_path } = data;

  return abs_path;
}
