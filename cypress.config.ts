import { addMatchImageSnapshotPlugin } from '@simonsmith/cypress-image-snapshot/plugin';
import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:3000',
    supportFile: 'cypress/support.ts',
    setupNodeEvents(on) {
      addMatchImageSnapshotPlugin(on);
    },
  },
  retries: 3,
  screenshotsFolder: 'cypress/debug',
  fixturesFolder: false,
  video: false,
});
